#!/bin/bash

git fetch --all
chmod +x TS3MusicBot_Control.sh TS3MusicBot_Control system/TS3MusicBot_Control
tar cfv ../TS3MusicBot_Control.tar TS3MusicBot_Control.sh TS3MusicBot_Control LICENSE CHANGELOG system/link.list system/colors system/version system/dbversion system/TS3MusicBot_Control system/lang/*
cp system/version ../
cp system/dbversion ../
cp install/install.sh ../
git reset --hard
git checkout release_$CI_COMMIT_REF_NAME
if [ -f TS3MusicBot_Control.tar ]; then rm TS3MusicBot_Control.tar; fi
if [ -f version ]; then rm version; fi
if [ -f dbversion ]; then rm dbversion; fi
if [ -f install.sh ]; then rm install.sh; fi
mv ../TS3MusicBot_Control.tar TS3MusicBot_Control.tar
mv ../version version
mv ../dbversion dbversion
mv ../install.sh install.sh
git add TS3MusicBot_Control.tar version dbversion install.sh
git commit -m '[skip ci] commit from CI runner'
git push --follow-tags origin release_$CI_COMMIT_REF_NAME

exit 0