# TS3MusicBot Control Script

### TS3MusicBot Homepage: https://ts3musicbot.net
[![TS3MusicBot_Logo](http://ts3musicbot.net/linkus/ts3musicbot1.png)](https://www.ts3musicbot.net)  [![TS3MusicBot_Logo2](http://ts3musicbot.net/linkus/TS3MusicBot_logo.gif)](https://www.ts3musicbot.net)

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/W7W814GV6)

**Current version:** ![Image4](https://img.shields.io/badge/Version-1.0.5-green.svg)

<img width="700" alt="Main_Menu" src="https://xbackbone.pkhw.de/36ed1/5d22e84276e31.png/raw">

---

# Table of Content

### [1. Attention](#1-attention-)

### [2. About](#2-about-)

### [3. Usage](#3-usage-)

### [4. Parameter](#4-parameter-)

### [5. Versions](#5-versions-)

### [6. Supportet OS](#6-supportet-operating-systems-install-script-)

### [7. Windows Subsystem](#7-windows-subsystem-)

### [8. Required Dependencies](#8-required-dependencies-automatic-installed-)

### [9. Latest Changes](#9-latest-changes-)

### [10. Some Pictures](#10-some-pictures-)

### [11. Support](#11-support-)


---
&nbsp; 

# 1. Attention [↑](#table-of-content)

**A license is required to use the Bot! Buy license here: https://ts3musicbot.net**

---
&nbsp; 

# 2. About [↑](#table-of-content)

**TS3MusicBot.net:**
> You ever wanted to play music on your teamspeak or discord server? Now you can!
> TS3MusicBot is a unique feature for your teamspeak or discord server fully working on linux and windows.
> 
> Upload music files, manage folders, play all kind of music files, stream live internet radio stations, direct playback of youtube, soundcloud and more links.
> The TS3MusicBot can be controlled with chat commands or with the build in webinterface.
> 
> Listen to music in groups while playing your favorite game. Let your friends listen to a youtube video you found.
> A new experience to listen music live with others in the same channel.

**This Script:**

* Controll Script for TS3MusicBot.
* With this Script you can Easy Start, Stop, Restart the Bot.
* You can Check the State of the Bot.
* You can Easy Join into the Screen Session.

The Script Create a User (TS3MusicBot_Control).

All Bots Start under this User.

### All Datas will save at /home/TS3MusicBot_Control/

---
&nbsp; 

# 3. Usage [↑](#table-of-content)

**1. Installation (Autoinstall):**

```
bash <(curl -sSL http://y.pkhw.de/ts3mbcontrolinstall)
```

**2. Start the Script after Installation with:**

```
TS3MusicBot_Control
```

---
&nbsp; 

# 4. Parameter [↑](#table-of-content)

### Arguments:

| Parameter  | Description                           |
|------------|---------------------------------------|
| -b         | Beta Mode for this Script
| -B         | Beta Mode for TS3MusicBot             |
| -d         | Debug Mode for this Script            |
| -D (Mode)* | Debug Mode for TS3MusicBot            |
| -N*²       | TS3MusicBot start in Notportable Mode |
| -h         | Display this help page                |

\* Available Modes:
* -debug-exec
* -debug-exec-client
* -debug-exec-player

\*² Notportable Mode:
* **For this the System Require some Dependencies! Only use this if you now what you do!**

### Options:

| Option          | Description                                         |
|-----------------|-----------------------------------------------------|
| start [PORT]*   | Start the TS3MusicBot                               |
| stop [PORT]*    | Stop the TS3MusicBot                                |
| restart [PORT]* | Restart the TS3MusicBot                             |
| status          | Display the Status of All TS3Music Bots             |
| update          | Check if a Newer Version of the Script is Available |
| help            | Display this help page                              |

\* Port
* Note: Not indicated Port are all bots start, stops or restart.

---
&nbsp;  

# 5. Versions [↑](#table-of-content)

| Master  | stable version                 |
|---------|--------------------------------|
| Beta    | testing version                |
| Develop | Developer Version [Don't Use!] |

---
&nbsp;  

# 6. Supportet Operating Systems (Install Script) [↑](#table-of-content)

| Debian | Ubuntu | Cent OS |
|:------:|:------:|:-------:|
|   11   |  20.04 |    8    |
|   10   |  18.04 |         |
|    9   |        |         |

---
&nbsp;  

# 7. Windows Subsystem [↑](#table-of-content)

If you will Use Windows Subsystem Checkout the Official Version: https://www.ts3musicbot.net/index.php?mode=account&tab=how_to_install

(You must be logged in)

Runs with TS3MusicBot_Control.

---
&nbsp;  

# 8. Required Dependencies (Automatic Installed) [↑](#table-of-content)

This Dependencies will be installed if not already exist:

**APT-GET:**
* openjdk-18-jre - openjdk-8-jre (The newest available)
* wget
* whiptail
* screen
* dnsutils
* lsof
* sqlite3
* libsqlite3-dev

**YUM**
* java-11-openjdk or java-1.8.0-openjdk or java-1.7.0-openjdk
* wget
* newt
* screen
* bind-utils
* lsof
* sqlite
* sqlite-devel

---
&nbsp;  

# 9. Latest Changes [↑](#table-of-content)

### Version 1.0.5 (13.08.2020)
* Edit. The Message "Press any Key to continue" only display if start in Graphical Menu (Start with "TS3MusicBot_Control" or "TS3MusicBot_Control menu"). Otherwise cronjob aborts.

### Version 1.0.4 (28.07.2020)
* Add Detection if TS3MusicBot is Updating on Start. (Set timeout to 5 Minutes)
* Fix Bot Start/Stop waiting Time. (Variable was used for several things :/)

### Version 1.0.3 (28.06.2020)

* Fix Special characters for Windows Subsystem.
* IP determination changed
* Change Client Check
* Webinterface check text customization

### Version 1.0.2

* Use Notportable in Windows Subsystem.


### Full Changelog: https://gitlab.com/philw95/ts3musicbot_control/blob/master/CHANGELOG

---
&nbsp;  

# 10. Some Pictures [↑](#table-of-content)

**Bot Info**

<img width="700" alt="Bot_Info" src="https://xbackbone.pkhw.de/36ed1/5d22e8fb07a11.png/raw">


**Bot Administration**

<img width="700" alt="Bot_Administration" src="https://xbackbone.pkhw.de/36ed1/5d22e90691dcb.png/raw">


**Change Bot Settings 1**

<img width="700" alt="Bot_Settings1" src="https://xbackbone.pkhw.de/36ed1/5d22e9432d900.png/raw">


**Change Bot Settings 2**

<img width="700" alt="Bot_Settings2" src="https://xbackbone.pkhw.de/36ed1/5d22e94d17cf2.png/raw">


**Extras**

<img width="700" alt="Extras" src="https://xbackbone.pkhw.de/36ed1/5d22e9626788f.png/raw">


**Bot State**

<img width="700" alt="Bot_State" src="https://xbackbone.pkhw.de/36ed1/5d22eeebb9769.png/raw">



---
&nbsp;  

# 11. Support [↑](#table-of-content)

### TS3MusicBot Livechat/Support:

[![Support](https://xbackbone.pkhw.cloud/36ed1/qOKoWOYI76.png/raw.png)](https://support.ts3musicbot.net/)
<a href="https://ts3musicbot.net"><img src="https://xbackbone.pkhw.de/36ed1/hANArIWozi187.png/raw" width="70" title="Website" alt="Website"></a>
<a href="mailto:support@ts3musicbot.de"><img src="https://xbackbone.pkhw.de/36ed1/nijAKIgEYA923.png/raw" width="70" title="Send E-Mail" alt="E-Mail"></a>


---
&nbsp;  
