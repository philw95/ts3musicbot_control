#!/bin/bash

# Path Variables part1
FILEPATH=$(readlink -f "$0")
MAIN_SCRIPT_PATH="${FILEPATH}"
BINARYPATH="$(dirname "${FILEPATH}")"
DATEINAME2="$(basename "${FILEPATH}")"
cd "${BINARYPATH}"
TMP="/tmp/TS3MusicBot_Control"
SYSTEMPATH="${BINARYPATH}/system"
SETTINGS_DB="settings.db"
SETTINGS_DB_PATH="${SYSTEMPATH}/${SETTINGS_DB}"

# Variables
ME=`whoami`
USERNAME="TS3MusicBot_Control"

# Source colors, link.list
source system/colors
source system/link.list

# Characters
if ! [ "$(uname -a | grep "Microsoft")" = "" ]; then
	GOOD="[${GREEN}V${MAIN}]"
	ERROR="[${RED}X${MAIN}]"
else
	GOOD="[${GREEN}✓${MAIN}]"
	ERROR="[${RED}✗${MAIN}]"
fi
NEUTRAL="[${YELLOW}-${MAIN}]"
INFO="[i]"


# Banner
printf "${BOLD}${LIGHTGREEN}  ________________ __  ___           _      ____        __ ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN} /_  __/ ___/__  //  |/  /_  _______(_)____/ __ )____  / /_${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}  / /  \__ \ /_ </ /|_/ / / / / ___/ / ___/ __  / __ \/ __/${MAIN}\n"
printf "${BOLD}${LIGHTGREEN} / /  ___/ /__/ / /  / / /_/ (__  ) / /__/ /_/ / /_/ / /_  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}/_/  /____/____/_/  /_/\__,_/____/_/\___/_____/\____/\__/  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}                                                           ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}          ______            __             __${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}         / ____/___  ____  / /__________  / /${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}        / /   / __ \/ __ \/ __/ ___/ __ \/ / ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}       / /___/ /_/ / / / / /_/ /  / /_/ / /  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}       \____/\____/_/ /_/\__/_/   \____/_/ ${MAIN}\n\n"

# Execute script as user only
if ! [ "${ME}" = "${USERNAME}" ]; then
	whiptail --backtitle "${BACKTITLE}" --title "${BACKTITLE} - User" --msgbox "Please run this Script as ${USERNAME}!" 8 78 3>&1 1>&2 2>&3
	exit 1
fi

# Create tmp & config folder if not Exist
if [ ! -d ${TMP} ]; then mkdir ${TMP}; fi

# functions
export_variables () {
# Script Variables
export USERNAME DATEINAME2 BETA DEVELOP VERSION DB_VERSION NAME NAME_SHORT BACKTITLE BACKTITLE_SHORT DEBUG GITLAB_DB_VERSION SETTINGS_DB
# Path Variables
export BINARYPATH LIBRARYPATH TMP MAIN_SCRIPT_PATH SETTINGS_DB_PATH SYSTEMPATH
# Characters
export GOOD ERROR NEUTRAL INFO
# Bot Variables
export BETAGLOBAL DEBUGGG STARTNOTPORTABLE
# Other
export WXYZ
}

export_functions () {
export -f print_dateiname debug check_exit good_edit error_edit bold spinner spinner_background
}

print_dateiname () {
printf "${LIGHTCYAN}[${DATEINAME}]${MAIN} "
}

debug () {
if [ "$DEBUG" = "1" ]; then
	printf "$1"
fi
}

check_exit () {
if ! [ "$?" = "0" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: ${1} Exit state: ${2} \n"
	exit 1
fi
}

good_edit () {
printf "\033[s\033[1A\r\033[1C${GOOD}\033[u"
}

error_edit () {
printf "\033[s\033[1A\r\033[1C${ERROR}\033[u"
}

bold () {
printf "${BOLD}${1}${MAIN}"
}

spinner () {
if [ "${zeile}" = "" ]; then zeile="1"; fi
if [ "${spalte}" = "" ]; then spalte="2"; fi
while true
do
  printf "\033[s\033[${zeile}A\r\033[${spalte}C${YELLOW}\\\\${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[${zeile}A\r\033[${spalte}C${YELLOW}|${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[${zeile}A\r\033[${spalte}C${YELLOW}/${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[${zeile}A\r\033[${spalte}C${YELLOW}-${MAIN}\033[u" ; sleep 0.15
done
}

spinner_background () {

PID="$!"
if [ "${NO_OUTPUT}" = "1" ]; then
	${1} &> /dev/null
else
	${1}
fi
EXITSTATE="$?"
if ! [ "${EXITSTATE}" = "0" ] && ! [ "${NO_EXIT}" = "1" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: '${2}' Exit state: ${EXITSTATE} \n"
	kill $PID
	wait $PID 2> /dev/null
	exit 1
elif ! [ "${EXITSTATE}" = "0" ] && [ "${NO_EXIT}" = "1" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: '${2}' Exit state: ${EXITSTATE} \n"
	kill $PID
	wait $PID 2> /dev/null
	return
fi
good_edit
kill $PID
wait $PID 2> /dev/null

}
# Example for spinner:
# spinner & spinner_background "apt-get update" "apt-get update"

# Help
help () {

clear
echo "Use: ${DATEINAME} [Argument] [Option] [Port]"
echo ""
echo "${DATEINAME}  | Start the Graphical menu of the Script"
echo ""
echo "Available Arguments:"
echo ""
echo "    -B           Beta Mode for TS3MusicBot"
echo "    -d           Debug Mode for this Script"
echo "    -D (Mode)    Debug Mode for TS3MusicBot"
echo "                  Available Modes:"
echo "                  -debug-exec         | Print All Debug output"
echo "                  -debug-exec-client  | Print Client Debug output Only"
echo "                  -debug-exec-player  | Print Player Debug output Only"
echo ""
echo "    -N           TS3MusicBot start in Notportable Mode"
echo "    -h           Display this help page"
echo ""
echo "Available Options:"
echo ""
echo "    start   (Port)   Start the TS3MusicBot *"
echo "    stop    (Port)   Stop the TS3MusicBot *"
echo "    restart (Port)   Restart the TS3MusicBot *"
echo "    status           Display the Status of All TS3Music Bots"
echo "    update           Check if a Newer Version of the Script is Available"
echo "    help             Display this help page"
echo "    menu             Start the Graphical menu of the Script"
echo ""
echo "  *Note: Not indicated Port are all bots start, stops or restart."
echo ""
exit 0

}

# Arguments
while getopts ":bBdD:Nhz" opt
do
	case $opt in
		b)
			# Beta Mode for this Script
			BETA="1"
			print_dateiname
			printf "[${GREEN}Arguments${MAIN}] Starting in Beta Mode!\n"
		;;
		B)
			# Beta Mode for TS3MusicBot
			BETAGLOBAL="1"
			print_dateiname
			printf "[${GREEN}Arguments${MAIN}] TS3MusicBot starting with argument: -beta\n"
		;;
		d)
			# Debug Modus für dieses Script
			DEBUG="1"
			print_dateiname
			printf "[${GREEN}Arguments${MAIN}] ${DATEINAME} starting in Debug Mode\n"
		;;
		D)
			# Debug Modus für TS3MusicBot
			DEBUGGG=$OPTARG
			if [ "${DEBUGGG}" = "-debug-exec" ] || [ "${DEBUGGG}" = "-debug-exec-client" ] || [ "${DEBUGGG}" = "-debug-exec-player" ]; then
				print_dateiname
				printf "[${GREEN}Arguments${MAIN}] TS3MusicBot starting with Debug Mode: ${DEBUGGG}\n"
			else
				print_dateiname
				printf "${ERROR} Wrong Debug Mode: ${DEBUGGG} (-h for help)\n"
				exit 1
			fi
		;;
		N)
			# Bot im Notportable Mode Starten
			STARTNOTPORTABLE="1"
			print_dateiname
			printf "[${GREEN}Arguments${MAIN}] TS3MusicBot starting with argument: -notpotable\n"
		;;
		h)
			help
		;;
		z)
			# Develop Mode for this Script
			DEVELOP="1"
			print_dateiname
			printf "[${GREEN}Arguments${MAIN}] Starting in Develop Mode!\n"
		;;
		\?)
			print_dateiname
			printf "${ERROR} Wrong argument: -$OPTARG (-h for help)\n"
			exit 1
		;;
		:)
			print_dateiname
			printf "${ERROR} -$OPTARG requires an mode. (-h for help)\n"
			exit 1
		;;
	esac
done

# remove all arguments from $@
shift $(($OPTIND-1))

# System Path (Main, Beta, Develop)
LIBRARYPATH="${SYSTEMPATH}"
VERSION=$(cat ${LIBRARYPATH}/version 2> /dev/null)
DB_VERSION=$(cat ${LIBRARYPATH}/dbversion 2> /dev/null)
NAME="TS3MusicBot_Control"
NAME_SHORT="TS3MB_Control"
BACKTITLE="${NAME} ${VERSION}"
BACKTITLE_SHORT="${NAME_SHORT} - ${VERSION}"

# Set link Version, DBversion, Download Link
if [ "${BETA}" = "1" ]; then GITLAB_VERSION="${GITLAB_VERSION_BETA}"; elif [ "${DEVELOP}" = "1" ]; then GITLAB_VERSION="${GITLAB_VERSION_DEVELOP}"; else GITLAB_VERSION="${GITLAB_VERSION_MAIN}"; fi
if [ "${BETA}" = "1" ]; then GITLAB_DB_VERSION="${GITLAB_DB_VERSION_BETA}"; elif [ "${DEVELOP}" = "1" ]; then GITLAB_DB_VERSION="${GITLAB_DB_VERSION_DEVELOP}"; else GITLAB_DB_VERSION="${GITLAB_DB_VERSION_MAIN}"; fi
if [ "${BETA}" = "1" ]; then DOWNLOAD_SCRIPT="${DOWNLOAD_SCRIPT_BETA}"; elif [ "${DEVELOP}" = "1" ]; then DOWNLOAD_SCRIPT="${DOWNLOAD_SCRIPT_DEVELOP}"; else DOWNLOAD_SCRIPT="${DOWNLOAD_SCRIPT_MAIN}"; fi

# Version Kontrolle
cd ${TMP}
if [ -f gitlabversion ]; then rm gitlabversion; fi
if [ -f gitlabdbversion ]; then rm gitlabdbversion; fi
HTTP200CHECK_GITLAB_VERSION=$(wget -T 2 -t 1 --server-response ${GITLAB_VERSION} -O gitlabversion 2>&1| grep -c 'HTTP/1.1 200 OK')
HTTP200CHECK_GITLAB_DB_VERSION=$(wget -T 2 -t 1 --server-response ${GITLAB_DB_VERSION} -O gitlabdbversion 2>&1| grep -c 'HTTP/1.1 200 OK')
cd ${BINARYPATH}

version_check() {
a=0
if [ "${HTTP200CHECK_GITLAB_VERSION}" = "1" ] && [ "${HTTP200CHECK_GITLAB_DB_VERSION}" = "1" ]; then
	if [ -d ${LIBRARYPATH} ] && [ "$(cat ${LIBRARYPATH}/version 2> /dev/null)" = "$(cat ${TMP}/gitlabversion 2> /dev/null)" ]; then
		printf " ${GOOD} ${NAME} is up to date. Version: $(cat ${LIBRARYPATH}/version 2> /dev/null)\n"
		sleep 2
	else
		printf " ${INFO} Current installed Version: $(cat ${LIBRARYPATH}/version 2> /dev/null) | New Version: $(cat ${TMP}/gitlabversion 2> /dev/null)\n"
		printf " ${INFO} A new version is available, do you want to download it now? (Y/n)"
		read -n 1
		echo
		echo
		if [ "$REPLY" = "n" ] || [ "$REPLY" = "N" ]; then
			printf " ${INFO} Not Updating now.\n\n"
			read -n 1 -s -r -p " [Press any key to continue]"
			echo
		else
			cd ${TMP}
			printf " ${INFO} Download New Script: ${ARCHIVE_NAME_SCRIPT}\n"
			NO_OUTPUT="1"
			spinner & spinner_background "wget -T 2 -t 5 ${DOWNLOAD_SCRIPT}" "wget"

			cd ${BINARYPATH}
			printf " ${INFO} Extracting Scriptfiles \n"
			spinner & spinner_background "tar xfv ${TMP}/${ARCHIVE_NAME_SCRIPT} --overwrite" "tar"

			cd ${TMP}
			printf " ${INFO} Delete ${ARCHIVE_NAME_SCRIPT} \n"
			spinner & spinner_background "rm ${ARCHIVE_NAME_SCRIPT}" "rm"
			NO_OUTPUT=""

			cd ${BINARYPATH}
			printf " ${INFO} Restart the Script now ... \n"
			sleep 3
			echo

			export_variables
			exec ./${DATEINAME2} $*
		fi
	fi
else
	printf "[${RED}ERROR${MAIN}] ${RED}${PROJECTPATH} is not reachable! Version check is skipped.${MAIN}\n"
	VERSIONSCHECKFAIL="1"
	read -n 1 -s -r -p " [Press any key to continue]"
	printf "\n"
fi

}
if [ "$1" = "" ] || [ "$1" = "menu" ]; then
	version_check
fi

WXYZ="true"

case "$1" in
	help)
		help
	;;
	update)
		version_check
		exit 0
	;;
esac

if ! [ "$(uname -a | grep "Microsoft")" = "" ]
then
	STARTNOTPORTABLE="1"
fi

# Open main script
cd ${LIBRARYPATH}
export_variables
export_functions
echo ""
script -q -c "exec ./TS3MusicBot_Control $*" /dev/null
cd ${BINARYPATH}

exit 0