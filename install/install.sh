#!/bin/bash

# Define colors for printf
MAIN='\033[0m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
FLASH='\033[5m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'

# Zeichen
GOOD="[${GREEN}✓${MAIN}]"
ERROR="[${RED}✗${MAIN}]"
NEUTRAL="[${YELLOW}-${MAIN}]"
INFO="[i]"

# function
check_exit () {
if ! [ "$?" = "0" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: ${1} Exit state: ${2} \n"
	exit 1
fi
}

good_edit () {
printf "\033[s\033[1A\r\033[1C${GOOD}\033[u"
}

error_edit () {
printf "\033[s\033[1A\r\033[1C${ERROR}\033[u"
}

bold () {
printf "${BOLD}${1}${MAIN}"
}

spinner () {
while true
do
  printf "\033[s\033[1A\r\033[2C${YELLOW}\\\\${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}|${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}/${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}-${MAIN}\033[u" ; sleep 0.15
done
}

spinner_background () {

PID="$!"
if [ "${NO_OUTPUT}" = "1" ]; then
	${1} &> /dev/null
else
	${1}
fi
EXITSTATE="$?"
if ! [ "${EXITSTATE}" = "0" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: '${2}' Exit state: ${EXITSTATE} \n"
	kill $PID
	wait $PID 2> /dev/null
	exit 1
fi
good_edit
kill $PID
wait $PID 2> /dev/null

}
# Example for spinner:
# spinner & spinner_background "apt-get update" "apt-get update"


# Export functions and Variables
export -f check_exit good_edit error_edit bold spinner spinner_background
export MAIN BOLD UNDERLINE FLASH RED GREEN ORANGE BLUE PURPLE CYAN LIGHTGRAY DARKGRAY LIGHTRED LIGHTGREEN YELLOW LIGHTBLUE LIGHTPURPLE LIGHTCYAN WHITE GOOD ERROR NEUTRAL INFO

# Banner
clear
printf "${BOLD}${LIGHTGREEN}  ________________ __  ___           _      ____        __ ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN} /_  __/ ___/__  //  |/  /_  _______(_)____/ __ )____  / /_${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}  / /  \__ \ /_ </ /|_/ / / / / ___/ / ___/ __  / __ \/ __/${MAIN}\n"
printf "${BOLD}${LIGHTGREEN} / /  ___/ /__/ / /  / / /_/ (__  ) / /__/ /_/ / /_/ / /_  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}/_/  /____/____/_/  /_/\__,_/____/_/\___/_____/\____/\__/  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}                                                           ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}          ______            __             __${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}         / ____/___  ____  / /__________  / /${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}        / /   / __ \/ __ \/ __/ ___/ __ \/ / ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}       / /___/ /_/ / / / / /_/ /  / /_/ / /  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}       \____/\____/_/ /_/\__/_/   \____/_/ ${MAIN}\n\n"

echo
if [ "$(id -u)" = "0" ]; then
        printf " ${GOOD} Root User \n"
else
        printf " ${ERROR} Not Root User! You must have Root Rights! Use 'su' or 'sudo -i' \n"
        exit 1
fi

whiptail --backtitle "TS3MusicBot_Control" --title "TS3MusicBot_Control" --msgbox "You need an Active License from www.ts3musicbot.net to use TS3MusicBot!" 8 78
printf " ${INFO} You need an Active License from www.ts3musicbot.net to use TS3MusicBot! \n"

USERNAME="TS3MusicBot_Control"
egrep "^${USERNAME}" /etc/passwd > /dev/null
if [ $? -eq 0 ]; then
	printf " ${GOOD} User ${USERNAME} Exist. \n"
	if [ -d /home/${USERNAME} ]; then
		printf " ${GOOD} Home directory Exist. \n"
	else
		printf " ${INFO} Home directory don't Exist! (Creating now) \n"
		mkdir /home/${USERNAME} ; check_exit "mkdir" "$?"
		chown ${USERNAME}:users /home/${USERNAME} ; check_exit "chown" "$?"
		good_edit
	fi
else
	printf " ${INFO} User ${USERNAME} Dosen't Exist! (Creating Now) \n"
	useradd -g users -d /home/TS3MusicBot_Control -m -s /bin/bash TS3MusicBot_Control 2> /dev/null ; check_exit "useradd" "$?"
	passwd -d TS3MusicBot_Control &> /dev/null ; check_exit "passwd" "$?"
	good_edit
fi

LINUX_VERSION=$(cat /etc/issue)
LINUX_VERSION2=$(cat /etc/*-release)

# Check wich OS is Installed
VARIABLE_OS=$(echo $LINUX_VERSION | grep -i 'Debian')
if [ -n "$VARIABLE_OS" ]; then
	OS="Debian"
	OS_VERSION=$(cat /etc/debian_version | awk -F. '{print $1}')
	if [ "$OS_VERSION" = "11" ] || [ "$OS_VERSION" = "10" ] || [ "$OS_VERSION" = "9" ]; then
		SUPPORTED_OS="OK"
	else
		printf " ${ERROR} This Script don't support your Debian Version! Detected OS: $OS $OS_VERSION \n"
	fi
fi
VARIABLE_OS=$(echo $LINUX_VERSION | grep -i 'Ubuntu')
if [ -n "$VARIABLE_OS" ]; then
	OS="Ubuntu"
	OS_VERSION=$(cat /etc/lsb-release | grep -i DISTRIB_RELEASE | grep -o '.....$')
	if [ "$OS_VERSION" = "18.04" ] || [ "$OS_VERSION" = "20.04" ]; then
		SUPPORTED_OS="OK"
	else
		printf " ${ERROR} This Script don't support your Ubuntu Version! Detected OS: $OS $OS_VERSION \n"
	fi
fi
VARIABLE_OS=$(echo $LINUX_VERSION | grep -i 'CentOS')
VARIABLE_OS2=$(echo $LINUX_VERSION2 | grep -i 'CentOS')
if [ -n "$VARIABLE_OS" ] || [ -n "$VARIABLE_OS2" ]; then
	OS="CentOS"
	OS_VERSION=$(cat /etc/redhat-release | awk '{print $3}' | grep -o '^.')
	OS_VERSION2=$(cat /etc/redhat-release | awk '{print $4}' | grep -o '^.')
	if [ "$OS_VERSION" = "8" ]; then
		SUPPORTED_OS="OK"
	elif [ "$OS_VERSION2" = "8" ]; then
		OS_VERSION="$OS_VERSION2"
		SUPPORTED_OS="OK"
	else
		printf " ${ERROR} This Script don't support your CentOS Version! Detected OS: $OS $OS_VERSION \n"
	fi
fi

if ! [ "$SUPPORTED_OS" = "OK" ]; then
	printf " ${ERROR} ERROR! Check OS Error! Detected OS: $OS $OS_VERSION \n"
	echo
	printf " Please make sure you have Install this Packages:\n\n"
	printf "APT-GET: openjdk-X-jre 'X=8-18' wget whiptail screen dnsutils sqlite3\n"
	printf "YUM    : java-X-openjdk 'X=1.8.0 or X=11-18' wget newt screen bind-utils sqlite\n"
	echo
	printf " [Press any key to continue]"
	read -n 1 -s -r
	echo
else
	printf " ${INFO} Detected OS: $OS $OS_VERSION \n"
	sleep 2
fi

# Package installer
PACKETCOUNTER=0
JAVAINSTALLED=""
JAVAVERSION=""
# pakages=()
if [ "$SUPPORTED_OS" = "OK" ]; then
	if [ "$OS" = "Debian" ] || [ "$OS" = "Ubuntu" ]; then
		# Update System
		echo
		bold " ${INFO} Update your System \n"

		NO_OUTPUT="1"
		printf " ${INFO} Step 1: Clean up \n"
		spinner & spinner_background "apt-get autoclean -y" "apt-get autoclean"

		printf " ${INFO} Step 2: Update apt cache \n"
		spinner & spinner_background "apt-get update" "apt-get update"

		printf " ${INFO} Step 3: Update packages "
		UPDATES=$(($(apt list --upgradable 2>/dev/null | wc -l) - 1))

		if [[ $UPDATES -eq 0 ]]
		then
			printf "| System is up to date \n"
			good_edit
		else
			printf "\n ${ERROR} System is Not up to date! There are $UPDATES Update(s). Install now? (Y/n)"
			read -N 1
			echo

			if [ "$REPLY" = "Y" ] || [ "$REPLY" = "y" ] || [ "$REPLY" = "J" ] || [ "$REPLY" = "j" ]
			then
				spinner & spinner_background "apt-get upgrade -y" "apt-get upgrade"
			else
				printf " ${ERROR} System is Not up to date! Exit Now.\n"
				exit 1
			fi
		fi
		
		# else
			# i=0
			# while [ $i -lt $UPDATES ]
			# do
				# ((i++))
				# pakage=$(apt list --upgradable 2>/dev/null | sed '1,1d' | sed -n "${i}p" | awk 'BEGIN { FS = "/" } ; {print $1}')
				# is_hold=$(apt-mark showhold ${pakage})
				# if [ "is_hold" = "" ]; then
					# pakages+=($pakage)
				# fi
			# done
			# if [ "${#pakages[@]}" -gt 0 ]
			# then
				# printf "\n ${ERROR} System is Not up to date! There are $UPDATES Update(s). Install now? (Y/n)"
				# read -N 1
				# echo

				# if [ "$REPLY" = "Y" ] || [ "$REPLY" = "y" ] || [ "$REPLY" = "J" ] || [ "$REPLY" = "j" ]
				# then
					# spinner & spinner_background "apt-get --only-upgrade install $pakages -y" "apt-get upgrade"
				# else
					# printf " ${ERROR} System is Not up to date! Exit Now.\n"
					# exit 1
				# fi
			# else
				# printf "| System is up to date \n"
				# good_edit
			# fi
		# fi

		printf " ${INFO} Step 4: Remove unused packages \n"
		spinner & spinner_background "apt-get --purge autoremove -y" "apt-get --purge autoremove"

		printf " ${INFO} Step 5: Clean up \n"
		spinner & spinner_background "apt-get autoclean -y" "apt-get autoclean"
		NO_OUTPUT=""

		echo

		bold " ${INFO} Check required dependencies \n"
		# Java
		PACKETLISTJAVA=( openjdk-18-jre openjdk-17-jre openjdk-16-jre openjdk-15-jre openjdk-14-jre openjdk-13-jre openjdk-12-jre openjdk-11-jre openjdk-10-jre openjdk-9-jre openjdk-8-jre )
		for PACKET in "${PACKETLISTJAVA[@]}"
		do
			if dpkg-query -W -f='${Status} ${Version}\n' $PACKET 2> /dev/null | grep -qw "ok installed"; then
				printf " ${GOOD} $PACKET \n"
				JAVAINSTALLED="1"
			fi
		done

		if [ ! "$JAVAINSTALLED" = "1" ]; then
			if apt-cache search openjdk | grep -qo "openjdk-[0-9]-jre " || apt-cache search openjdk | grep -qo "openjdk-[0-9][0-9]-jre "; then
				for PACKET in "${PACKETLISTJAVA[@]}"
				do
					if apt-cache search openjdk | grep -qo "$PACKET "; then
						JAVAVERSION="${PACKET}"
						break
					fi
				done
			fi
		fi

		# Packete prüfen, wenn nicht vorhanden installieren.
		PACKETLIST=( wget whiptail screen dnsutils lsof sqlite3 libsqlite3-dev $JAVAVERSION )
		for PACKET in "${PACKETLIST[@]}"
		do
			if dpkg-query -W -f='${Status} ${Version}\n' $PACKET 2> /dev/null | grep -qw "ok installed"; then
				printf " ${GOOD} $PACKET \n"
			else
				printf " ${ERROR} $PACKET installing \n"
				NO_OUTPUT="1"
				spinner & spinner_background "apt-get install -y $PACKET" "apt-get install"
				NO_OUTPUT=""
				sleep 1
			fi
		done

		sleep 2
	elif [ "$OS" = "CentOS" ]; then
		# Update System
		printf " ${INFO} Update your System: \n"

		NO_OUTPUT="1"
		printf " ${INFO} Step 1: Cleanup \n"
		spinner & spinner_background "yum clean all" "yum clean all"

		printf " ${INFO} Step 2: Update cache \n"
		spinner & spinner_background "yum -q makecache" "yum makecache"

		printf " ${INFO} Step 3: Update Packages "
		UPDATES=$(($(yum check-update 2>/dev/null | wc -l) - 2))

		if [[ $UPDATES -eq 0 ]] || [[ $UPDATES -eq -1 ]]
		then
			printf "| System is up to date \n"
			good_edit
		else
			printf "\n ${ERROR} System is Not up to date! There are $UPDATES Update(s). Install now? (Y/n)"
			read -N 1
			echo

			if [ "$REPLY" = "Y" ] || [ "$REPLY" = "y" ] || [ "$REPLY" = "J" ] || [ "$REPLY" = "j" ]
			then
				spinner & spinner_background "yum -q -y update" "yum update"
			else
				printf " ${ERROR} System is Not up to date! Exit Now.\n"
				exit 1
			fi
		fi

		printf " ${INFO} Step 4: Remove unused packages \n"
		spinner & spinner_background "yum -q -y autoremove" "yum autoremove"

		printf " ${INFO} Step 5: Cleanup \n"
		spinner & spinner_background "yum clean all" "yum clean all"
		NO_OUTPUT=""

		echo

		# Java
		PACKETLISTJAVA=( java-11-openjdk java-1.8.0-openjdk java-1.7.0-openjdk )
		for PACKET in "${PACKETLISTJAVA[@]}"
		do
			if rpm -q $PACKET &> /dev/null; then
				printf " ${GOOD} $PACKET installed. \n"
				JAVAINSTALLED="1"
			fi
		done

		if [ ! "$JAVAINSTALLED" = "1" ]; then
			for PACKET in "${PACKETLISTJAVA[@]}"
			do
				if yum list java-* | grep -qm 1 "$PACKET"; then
					JAVAVERSION="${PACKET}"
					break
				fi
			done
		fi

		PACKETLIST=( wget newt screen bind-utils lsof sqlite sqlite-devel $JAVAVERSION )
		for PACKET in "${PACKETLIST[@]}"
		do
			if rpm -q $PACKET &> /dev/null; then
				printf " ${GOOD} $PACKET installed \n"
			else
				printf " ${ERROR} $PACKET installing \n"
				NO_OUTPUT="1"
				spinner & spinner_background "yum install -y $PACKET" "yum install"
				NO_OUTPUT=""
				sleep 1
			fi
		done

		sleep 2
	fi
fi
echo

cd /home/${USERNAME}
if [ -f TS3MusicBot_Control.sh ]; then
	printf " ${GOOD} TS3MusicBot_Control.sh Already Exist! \n"
else
	bold " ${INFO} Download TS3MusicBot_Control \n"

	NO_OUTPUT="1"
	printf " ${INFO} Download TS3MusicBot_Control.tar \n"
	spinner & spinner_background "wget -T 2 -t 5 https://gitlab.com/philw95/ts3musicbot_control/raw/release_master/TS3MusicBot_Control.tar" "wget"

	printf " ${INFO} Extracting TS3MusicBot_Control.tar \n"
	spinner & spinner_background "tar xfv TS3MusicBot_Control.tar" "tar"

	printf " ${INFO} Delete TS3MusicBot_Control.tar \n"
	spinner & spinner_background "rm TS3MusicBot_Control.tar" "rm"

	printf " ${INFO} CHOWN files to ${USERNAME} \n"
	spinner & spinner_background "chown -R ${USERNAME} /home/TS3MusicBot_Control" "chown"
	NO_OUTPUT=""

	echo
fi

if [ -f /tmp/TS3MusicBot_Notportable_Mode ]; then touch /home/${USERNAME}/system/notportable; chown ${USERNAME}:users /home/${USERNAME}/system/notportable; rm /tmp/TS3MusicBot_Notportable_Mode; fi

printf " ${INFO} Create Link. (/usr/local/bin/TS3MusicBot_Control) \n"
NO_OUTPUT="1"
spinner & spinner_background "cp /home/${USERNAME}/TS3MusicBot_Control /usr/local/bin/" "cp"
NO_OUTPUT=""
echo

echo "####################"
echo
printf " ${GOOD} Finish \n"
echo
printf " Now use 'TS3MusicBot_Control' to start the Script. \n"

echo

exit 0